# Sierpinski Diamond

A Sierpinski Diamond generator. Takes diamond height, and desired recursive level as inputs.

![screenshot](https://gitlab.com/fbarn/sierpinski-diamond/raw/master/demo.png)
