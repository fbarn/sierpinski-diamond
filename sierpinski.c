#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

void sierpinski_t(int dheight, int l){
        int h = (dheight+1)/2;
        int length = dheight;
        int m[h][length];
        for(int i = 0; i < h; i++){
                for (int j=0; j<length; j++){
                        m[i][j]=0;
                }
        }

        int x=(length-1)/2;
        int y=0;
        void sierpinskihelper(int x, int y,int h, int l){
                if (l==1){
                        int count=0;
                        for(int i = y; i < y+h; i++){
                                if (i==y){
                                        int count=1;
                                        m[i][x]=1;
                                }
                                if (i>y){
                                        count+=1;
                                        for (int j=x-count; j<x+count+1; j++){
                                                m[i][j]=1;
                                                }
                                }
                        }
                }
                else{
                        sierpinskihelper(x,y,h/2,l-1);
                        sierpinskihelper(x+h/2,y+h/2,h/2,l-1);
                        sierpinskihelper(x-h/2,y+h/2,h/2,l-1);
                }
        }
        sierpinskihelper(x,y,h, l);
        for(int i = 0; i < h; i++){
                for (int j=0; j<length; j++){
                        if (m[i][j]==1){
                                printf("%s", "*");
                        }
                        else{
                                printf("%s"," ");
                        }
                }
                printf("\n");
        }
        for (int i =h-2; i>=0; i--){
                for (int j=0; j<length; j++){
                        if (m[i][j]==1){
                                printf("%s", "*");
                        }
                        else{
                                printf("%s"," ");
                        }
                }
                printf("\n");

        }

}
int power(int base, int exp){
        int _base=base;
        if(exp==0){
                return 1;
        }
        else{
                for (int i=0; i<exp-1; i++){
                        base*=_base;
                }
                return base;
        }
}
int isPowerOfTwo (int x){
         while (((x % 2) == 0) && x > 1){
                    x /= 2;}
                    return (x != 1);
}

void main(int argc, char *argv[]){
        if (argc!=3){
                printf("%s\n", "ERROR: Wrong number of arguments. Two required");
                return;
        }
        int height=atoi(argv[1]);
        int heightceil=(height+1)/2;
        int level=atoi(argv[2]);
        if (height%2==0 | height<0 | level<=0){
                printf("%s\n", "ERROR: Bad argument. Height must be a positive odd integer. Level must be greater than 0");
        }
        else if (heightceil<power(2, level-1) | isPowerOfTwo(heightceil)){
                printf("%s\n", "ERROR: Height does not allow evenly dividing requested number of levels.");
        }
        else{
                sierpinski_t(height, level);
        }
}
